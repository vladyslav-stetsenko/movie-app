import './App.css';
import Header from "./components/Header/Header";
import CardsList from "./components/CardsList/CardsList";
import Footer from "./components/Footer/Footer";

function App() {
  const cardsList = [
    {
      imageUrl: '/Bitmap.png',
      title: 'Bohemian Rhapsody',
      category: 'Drama, Biography, Music',
      year: '2003',
    },
    {
      imageUrl: '/Bitmap.png',
      title: 'Bohemian Rhapsody',
      category: 'Drama, Biography, Music',
      year: '2003',
    },
    {
      imageUrl: '/Bitmap.png',
      title: 'Bohemian Rhapsody',
      category: 'Drama, Biography, Music',
      year: '2003',
    },
    {
      imageUrl: '/Bitmap.png',
      title: 'Bohemian Rhapsody',
      category: 'Drama, Biography, Music',
      year: '2003',
    },
    {
      imageUrl: '/Bitmap.png',
      title: 'Bohemian Rhapsody',
      category: 'Drama, Biography, Music',
      year: '2003',
    },
    {
      imageUrl: '/Bitmap (3).png',
      title: 'Pulp Fiction',
      category: 'Action & Adventure',
      year: '2004',
    },
  ];

  const filterItems = ['Documentary', 'Comedy', 'Horror', 'Crime'];

  return (
    <div className="App">
      <Header/>
      <CardsList listItems={cardsList} filterItems={filterItems}/>
      <Footer/>
    </div>
  );
}

export default App;
