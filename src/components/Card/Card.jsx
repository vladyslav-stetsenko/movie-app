import React from 'react';
import styles from "./Card.module.scss";
import {BsThreeDotsVertical} from "react-icons/all";

const Card = ({imageUrl, cardTitle, category, releaseDate}) => {
  return (
    <div className={styles.card}>
      <img src={imageUrl} alt=""/>
      <div className={styles.titleWrapper}>
        <span className={styles.title}>{cardTitle}</span>
        <span className={styles.year}>{releaseDate}</span>
      </div>
      <span className={styles.type}>{category}</span>
      <div className={styles.infoIcon}>
        <BsThreeDotsVertical className={styles.icon}/>
      </div>
    </div>
  );
};

export default Card;
