import React, {useState} from 'react';
import PropTypes from 'prop-types';
import styles from './CardsList.module.scss';
import {AiFillCaretDown} from "react-icons/all";
import Card from "../Card/Card";

const CardsList = ({listItems, filterItems}) => {

  const [filter, setFilter] = useState(0);

  const onFilterClick = (index) => {
    setFilter(index)
  };

  return (
    <div className={styles.list}>
      <div className={styles.listHeader}>
        <div className={styles.filter}>
          <div className={`${styles.item} ${0 === filter ? styles.active : ''}`} onClick={()=> onFilterClick(0)}>All</div>
          {filterItems.map((item, index) => <div key={index + 1} onClick={() => onFilterClick(index + 1)} className={`${styles.item} ${index + 1 === filter ? styles.active : ''}`}>{item}</div>)}
        </div>
        <div className={styles.dropdown}>
          <div className={styles.tag}>Sort by</div>
          <div className={styles.options}>Release date</div>
          <AiFillCaretDown className={styles.icon}/>
        </div>
      </div>
      <div className={styles.totalItems}>
        <p><span>{listItems.length}</span> {listItems.length === 1 ? 'movie' : 'movies'} found</p>
      </div>
      <div className={styles.cardsList}>
        {
          listItems.map((card, index) =>
            <Card key={index}
                  cardTitle={card.title}
                  category={card.category}
                  imageUrl={card.imageUrl}
                  releaseDate={card.year}
            />
          )
        }
      </div>
    </div>
  );
};

CardsList.propTypes = {
  listItems: PropTypes.array,
  filterItems: PropTypes.array,
}

export default CardsList;
