import React from 'react';
import styles from './Header.module.scss';
import Search from "../Search/Search";

const Header = () => {
  return (
    <div className={styles.header}>
      <div className={styles.background} style={{backgroundImage: "url(/header-image.png)"}}/>
      <div className={styles.foreground}>
        <div className={styles.topSection}>
          <div className={styles.icon}>
            <span style={{fontWeight: 'bold'}}>netflix</span><span>roulette</span>
          </div>
          <button className={styles.addMovieButton}>
            + ADD MOVIE
          </button>
        </div>
        <div className={styles.middleSection}>
          <h2 className={styles.title}>Find your movie</h2>
          <Search/>
        </div>
      </div>
    </div>
  );
};

export default Header;
