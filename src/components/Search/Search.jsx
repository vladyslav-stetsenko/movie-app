import React from 'react';
import styles from "./Search.module.scss";

const Search = () => {
  return (
    <div className={styles.search}>
      <input className={styles.input} placeholder={"What do you want to watch?"} type="text"/>
      <button className={styles.searchButton}>Search</button>
    </div>
  );
};

export default Search;
